# Release Keys

## Generating the key

Use the following command to generate your key file:
```
gpg --armor --export KEY_NAME > keys/USERNAME@key1.asc
```

To find out which KEY_NAME to use, you can run:

```
gpg --list-keys
```

